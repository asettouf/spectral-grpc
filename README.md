To start the project:

Prerequisites:

* Python 3.7 (not tested with other versions)
* npm 6.14 (not tested with other versions, but should work as well)

Steps:

1. Create your python virtual env
2. Install the python dependencies (in grpc/requirements.txt, with the command _pip install -r grpc/requirements.txt_)
3. Install the npm dependencies (to have a http server running the HTML, _cd front && npm install && cd .._)
4. Run _./start_all.sh_  (it should start the backend, the proxy, and the frontend)
5. Go to http://localhost:8080/ and check the JSON is displayed properly


Notes

* This should mostly be considered as a very early protoype as the packaging is not at all handled
* At each start of the grpc server we regenerate the *pb2.py as we want to have the latest version of the .proto file, be aware it might change
* We generate "duplicates" of the *pb2 files, however it is made so as in the future the proxy and the backend might be located on different servers
* The current display is minimalistic as it is a showcase it is feasible, we are aware that it is not the final product
* For simplicity the testing data is included in the repository, however it shall not be the case in the future
* Tested with Firefox 77.0 and Chromium 80.0