// As we serve http in a very simple fashion, we didn't include a packaging phase like we would normally do and directly fetch jquery through a CDN
import 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js';

const $ = window.$;

/**
 * Retrieve the time series from the grpc proxy
 * Fills the div with id "table" on success
 * Log an error in the console on failure
 */
let retrieveTimeseries = function() {
    $.getJSON("http://localhost:5000/api/timeseries").then(fillTable).catch(console.error);
}

let fillTable = function(data) {
    $("#table").html("<pre>" + data + "</pre>");
}

let main = function(){
    retrieveTimeseries();
}


main();