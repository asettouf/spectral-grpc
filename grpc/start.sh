#!/usr/bin/env bash 

# Create the pb2 files
./init.sh

# Start the grpc backend and the grpc proxy 
cd server/ && python main.py &
cd client/ && python main.py && cd ..