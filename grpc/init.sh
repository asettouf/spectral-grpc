#!/usr/bin/env bash 

# Create the pb2.py file needed

python -m grpc_tools.protoc -I./proto --python_out=./client/ --grpc_python_out=./client/ ./proto/data_sender.proto 
python -m grpc_tools.protoc -I./proto --python_out=./server/ --grpc_python_out=./server/ ./proto/data_sender.proto 

