
from concurrent import futures
import grpc
import csv
import json
import configparser

import data_sender_pb2
import data_sender_pb2_grpc

#Global variables declaration
config = configparser.ConfigParser()
config.read("config.ini")

# This class represent the timeseries protobuf used for the grpc interaction
class TimeSeries(data_sender_pb2_grpc.TimeSeriesServicer):
    
    def sendTimeSeries(self, request, context):
        reply = json.dumps(read_timeseries_as_dict())
        return data_sender_pb2.TimeSeriesReply(message=reply)

# Read the timeseries file and return a dict parsed from the csv
def read_timeseries_as_dict():
    with open(config["data"]["file"], "r") as meterusage_csv:
        csvReader = csv.DictReader(meterusage_csv)
        data = []
        for row in csvReader:
            data.append(row)
        return data

# Configure the server running
def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    data_sender_pb2_grpc.add_TimeSeriesServicer_to_server(TimeSeries(), server)
    server.add_insecure_port(config["local"]["server"] + ":" + config["local"]["port"])
    server.start()
    server.wait_for_termination()

if __name__ == "__main__":
    serve()
