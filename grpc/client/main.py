
import grpc
import flask
from flask import request, jsonify
from flask_cors import CORS
import configparser

import data_sender_pb2_grpc
import data_sender_pb2

#Global variables declaration
app = flask.Flask(__name__)
CORS(app)

config = configparser.ConfigParser()
config.read("config.ini")

# Request the timeseries from the grpc backend
def request_timeseries():
    with grpc.insecure_channel(config["backend"]["server"] + ":" + config["backend"]["port"]) as channel:
        stub = data_sender_pb2_grpc.TimeSeriesStub(channel)
        response = stub.sendTimeSeries(data_sender_pb2.TimeSeriesRequest())
    return response.message

@app.route('/api/timeseries', methods=['GET'])
def api_all():
    return jsonify(request_timeseries())

if __name__ == '__main__':
    app.run(host=config["local"]["server"], port=config["local"]["port"])