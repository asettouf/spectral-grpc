#!/usr/bin/env bash 

# start the grpc env
cd grpc/ && ./start.sh & 
# start the http server
cd front && npm run serve